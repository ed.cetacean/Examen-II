
from django.urls import path
from Home import views

app_name = "Home"
urlpatterns =[
    path('', views.Index.as_view(), name="Index"),
]

# ---------------------------------------------------------------------------- #
