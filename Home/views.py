
from django.views import generic
from django.shortcuts import render

## INDEX:
class Index(generic.View):
    template_name = "Home/Index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "Name" : "Ed Rubio",
        }
        return render(request, self.template_name, self.context)