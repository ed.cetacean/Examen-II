# Generated by Django 4.2.7 on 2023-11-05 02:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ciudad',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('NombreCiudad', models.CharField(max_length=80)),
            ],
        ),
        migrations.CreateModel(
            name='Estadio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('NombreEstadio', models.CharField(max_length=80)),
                ('Ciudad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Core.ciudad')),
            ],
        ),
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('NombreEquipo', models.CharField(max_length=80)),
                ('Estadio', models.CharField(choices=[('MetLife', 'MetLife'), ('Staples Center', 'Staples Center'), ('Soldier Field', 'Soldier Field'), ('AT&T Stadium', 'AT&T Stadium'), ('Hard Rock', 'Hard Rock')], max_length=80)),
                ('Ciudad', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Core.ciudad')),
            ],
        ),
    ]
