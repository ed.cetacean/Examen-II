# Generated by Django 4.2.7 on 2023-11-05 18:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Core', '0003_ciudad_descripcionciudad'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipo',
            name='DescripcionEquipo',
            field=models.CharField(default='Descripción no asignada.', max_length=120),
        ),
        migrations.AddField(
            model_name='estadio',
            name='DescripcionEstadio',
            field=models.CharField(default='Descripción no asignada.', max_length=120),
        ),
        migrations.AlterField(
            model_name='ciudad',
            name='DescripcionCiudad',
            field=models.CharField(default='Descripción no asignada.', max_length=120),
        ),
        migrations.AlterField(
            model_name='ciudad',
            name='NombreCiudad',
            field=models.CharField(default='Nombre no asignado.', max_length=80),
        ),
        migrations.AlterField(
            model_name='equipo',
            name='Ciudad',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='Core.ciudad'),
        ),
        migrations.AlterField(
            model_name='equipo',
            name='NombreEquipo',
            field=models.CharField(default='Nombre no asignado.', max_length=80),
        ),
        migrations.AlterField(
            model_name='estadio',
            name='NombreEstadio',
            field=models.CharField(default='Nombre no asignado.', max_length=80),
        ),
    ]
