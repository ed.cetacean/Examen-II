
from django import forms
from .models import Ciudad, Estadio, Equipo

## ESTADIO: ----------------------------------------------------------------- ##

class EstadioForm(forms.ModelForm):
    Ciudad = forms.ModelChoiceField(queryset=Ciudad.objects.all(),
        empty_label="Selecciona una ciudad",
        widget=forms.Select(attrs={"class": "form-select-ancho"})
    )

    class Meta:
        model = Estadio
        fields = ['NombreEstadio', 'DescripcionEstadio', 'Ciudad']

        widgets = {
            "NombreEstadio": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del estadio"}),
            "DescripcionEstadio": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Descripción del estadio"}),
        }

class UpdateEstadioForm(forms.ModelForm):
    Ciudad = forms.ModelChoiceField(queryset=Ciudad.objects.all(),
        empty_label="Selecciona una ciudad",
        widget=forms.Select(attrs={"class": "form-select-ancho"})
    )

    class Meta:
        model = Estadio
        fields = ['NombreEstadio', 'DescripcionEstadio', 'Ciudad']

        widgets = {
            "NombreEstadio": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del estadio"}),
            "DescripcionEstadio": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Descripción del estadio"}),
        }

## CIUDAD: ------------------------------------------------------------------ ##

class CiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ['NombreCiudad', 'DescripcionCiudad']

        widgets = {
            "NombreCiudad" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre",}),
            "DescripcionCiudad" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción"})
        }

class UpdateCiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ['NombreCiudad', 'DescripcionCiudad']

        widgets = {
            "NombreCiudad" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre",}),
            "DescripcionCiudad" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción"})
        }

## EQUIPO: ------------------------------------------------------------------ ##

class EquipoForm(forms.ModelForm):
    Estadio = forms.ModelChoiceField(queryset=Estadio.objects.all(),
        empty_label="Selecciona un estadio",
        widget=forms.Select(attrs={"class": "form-select-ancho"})
    )

    class Meta:
        model = Equipo
        fields = ['NombreEquipo', 'DescripcionEquipo', 'Estadio']

        widgets = {
            'NombreEquipo': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del equipo"}),
            'DescripcionEquipo': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción del equipo"}),
        }

class UpdateEquipoForm(forms.ModelForm):
    Estadio = forms.ModelChoiceField(queryset=Estadio.objects.all(),
        empty_label="Selecciona un estadio",
        widget=forms.Select(attrs={"class": "form-select-ancho"})
    )

    class Meta:
        model = Equipo
        fields = ['NombreEquipo', 'DescripcionEquipo', 'Estadio']

        widgets = {
            'NombreEquipo': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre del equipo"}),
            'DescripcionEquipo': forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción del equipo"}),
        }