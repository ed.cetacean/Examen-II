
from django.views import generic
from django.shortcuts import render
from django.urls import reverse_lazy

from .models import Ciudad, Estadio, Equipo
from .forms import CiudadForm, EstadioForm, EquipoForm, UpdateCiudadForm, UpdateEstadioForm, UpdateEquipoForm

## ESTADIO (CRUD): ---------------------------------------------------------- ##

## CREATE:
class CreateEstadio(generic.CreateView):
    model = Estadio
    form_class = EstadioForm
    template_name = "Core/EstadioCreate.html"
    success_url = reverse_lazy('Core:EstadioList')

## RETRIEVE (LIST - ESTADIO):
class ListEstadio(generic.View):
    template_name = "Core/EstadioList.html"

    def get(self, request, *args, **kwargs):
        queryset = Estadio.objects.all()

        self.context = {
            "EstadioQ": queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateEstadio(generic.UpdateView):
    model = Estadio
    form_class = UpdateEstadioForm
    template_name = "Core/EstadioUpdate.html"
    success_url = reverse_lazy("Core:EstadioList")

## DELETE:
class DeleteEstadio(generic.DeleteView):
    model = Estadio
    template_name = "Core/EstadioDelete.html"
    success_url = reverse_lazy("Core:EstadioList")

## DETAIL:
class DetailEstadio(generic.DetailView):
    template_name = "Core/EstadioDetail.html"
    model = Estadio

## CIUDAD: ------------------------------------------------------------------ ##

## CREATE:
class CreateCiudad(generic.CreateView):
    model = Ciudad
    form_class = CiudadForm
    template_name = "Core/CiudadCreate.html"
    success_url = reverse_lazy('Core:CiudadList')

## RETRIEVE (LIST - ESTADIO):
class ListCiudad(generic.View):
    template_name = "Core/CiudadList.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Ciudad.objects.all()

        self.context = {
            "CiudadQ" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateCiudad(generic.UpdateView):
    model = Ciudad
    form_class = UpdateCiudadForm
    template_name = "Core/CiudadUpdate.html"
    success_url = reverse_lazy("Core:CiudadList")

## DELETE:
class DeleteCiudad(generic.DeleteView):
    model = Ciudad
    template_name = "Core/CiudadDelete.html"
    success_url = reverse_lazy("Core:CiudadList")

## DETAIL:
class DetailCiudad(generic.DetailView):
    template_name = "Core/CiudadDetail.html"
    model = Ciudad

## EQUIPO: ------------------------------------------------------------------ ##

class CreateEquipo(generic.CreateView):
    model = Equipo
    form_class = EquipoForm
    template_name = "Core/EquipoCreate.html"
    success_url = reverse_lazy('Core:EquipoList')

class ListEquipo(generic.View):
    template_name = "Core/EquipoList.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Equipo.objects.all()

        self.context = {
            "EquipoQ" : queryset
        }
        return render(request, self.template_name, self.context)

class UpdateEquipo(generic.UpdateView):
    model = Equipo
    form_class = UpdateEquipoForm
    template_name = "Core/EquipoUpdate.html"
    success_url = reverse_lazy('Core:EquipoList')

class DeleteEquipo(generic.DeleteView):
    model = Equipo
    template_name = "Core/EquipoDelete.html"
    success_url = reverse_lazy('Core:EquipoList')

## DETAIL:
class DetailEquipo(generic.DetailView):
    template_name = "Core/EquipoDetail.html"
    model = Equipo