
from django.contrib import admin
from .models import Ciudad, Estadio, Equipo
from .forms import CiudadForm, EstadioForm, EquipoForm

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    form = CiudadForm
    list_display = ('NombreCiudad',)

@admin.register(Estadio)
class EstadioAdmin(admin.ModelAdmin):
    form = EstadioForm
    list_display = ('NombreEstadio', 'Ciudad')

@admin.register(Equipo)
class EquipoAdmin(admin.ModelAdmin):
    form = EquipoForm
    list_display = ('NombreEquipo', 'Estadio', 'Ciudad')